﻿- Tűzfal kikapcsolása (magánhálózat)
- Router port (nat) forward: 
	- Service type - HTTP
	- Ext. port - 80
	- Internal IP - (192.168....)
	- Internal port - 8000
	- Protocol - ALL
(cmd (admin) netsh http add iplisten ipaddress=192.168... && public ip)

- Website: ng serve --host=0.0.0.0
   	   - elérés: public ip:88 port (91.83.32.186:88)
	   - port forward: server - 80 -> 8000
 			   web - 88 -> 4200