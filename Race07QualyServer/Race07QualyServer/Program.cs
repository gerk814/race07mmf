﻿using System;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;

namespace Race07QualyServer
{
    public class Program
    {
        private static readonly string url = ConfigurationManager.AppSettings["url"];

        public static void Main()
        {
            // Create a Http server and start listening for incoming connections
            HttpServer.listener = new HttpListener();
            HttpServer.listener.Prefixes.Add(url);
            HttpServer.listener.Start();
           
            Console.WriteLine("Listening for connections on {0}", url);

            // Handle requests
            Task listenTask = HttpServer.HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();

            // Close the listener
            HttpServer.listener.Close();
        }
    }
}
