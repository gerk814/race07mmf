﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Race07QualyServer
{
    public class ServerDataStore
    {
        public Dictionary<string, PlayerData> Data { get; private set; } = new Dictionary<string, PlayerData>();

        public void HandleIncomingData(string username, string lastlap, string bestlap, int completedlaps)
        {
            if (string.IsNullOrEmpty(username))
            {
                return;
            }

            if (!Data.ContainsKey(username))
            {
                var newPlayer = new PlayerData { LapsCompleted = completedlaps, BestLap = bestlap };
                if (!string.IsNullOrEmpty(lastlap))
                {
                    newPlayer.Laps.Add(lastlap);
                }
                Data.Add(username, newPlayer);
            }

            if (bestlap != Data[username].BestLap)
            {
                Data[username].BestLap = bestlap;
            }

            if (completedlaps > Data[username].LapsCompleted || lastlap != Data[username].Laps.Last())
            {
                Data[username].LapsCompleted = completedlaps;
                Data[username].Laps.Add(lastlap);
            }
            else if (completedlaps == 0)
            {
                Data[username].Laps.Clear();
                Data[username].BestLap = string.Empty;
            }
        }

        public string ReturnDataMessage()
        {
            StringBuilder sb = new StringBuilder();
            var sortedList = Data.OrderBy(x => x.Value.BestLap);
            foreach (var player in sortedList)
            {
                sb.Append($"#{player.Key};{player.Value.LastLap};{player.Value.BestLap}");
            }

            return sb.ToString();
        }
    }

    public class PlayerData
    {
        public int LapsCompleted { get; set; } = 0;
        public string BestLap { get; set; } = "";
        public List<string> Laps { get; set; } = new List<string>();

        public string LastLap => Laps.Count > 0 ? Laps.Last() : string.Empty;
    }
}
