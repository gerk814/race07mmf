﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Collections.Specialized;

namespace Race07QualyServer
{
    public class HttpServer
    {
        public static HttpListener listener;
        public static int requestCount = 0;
        private static readonly ServerDataStore _dataStore = new ServerDataStore();

        public static async Task HandleIncomingConnections()
        {
            bool runServer = true;

            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (runServer)
            {
                // Will wait here until we hear from a connection
                HttpListenerContext ctx = await listener.GetContextAsync();

                // Peel out the requests and response objects
                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;

                try
                {
                    // Print out some info about the request
                    Console.WriteLine("Request #: {0}", ++requestCount);
                    Console.WriteLine(req.Url.ToString());
                    Console.WriteLine(req.HttpMethod);
                    Console.WriteLine(req.UserHostName);
                    Console.WriteLine(req.UserAgent);
                    
                    var parsed = HttpUtility.ParseQueryString(req.Url.Query.ToString());
                    var cmd = parsed.Get("command").ParseEnum();
                    string response = "";
                    switch (cmd)
                    {
                        case CommandType.AddLap:
                            response = AddLap(parsed);
                            break;
                        case CommandType.LeaderBoard:
                            response = LeaderBoard();
                            break;
                        default:
                            break;
                    }
                                       
                    Console.WriteLine();

                    // Write the response info
                    byte[] data = Encoding.UTF8.GetBytes(response);
                    resp.ContentEncoding = Encoding.UTF8;
                    resp.ContentLength64 = data.LongLength;
                    // Write out to the response stream (asynchronously), then close it
                    await resp.OutputStream.WriteAsync(data, 0, data.Length);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
                resp.Close();
            }
        }

        private static string LeaderBoard()
        {
            return _dataStore.ReturnDataMessage();
        }

        private static string AddLap(NameValueCollection parsed)
        {
            string username = parsed.Get("username");
            string lastlap = parsed.Get("lastlap");
            string bestlap = parsed.Get("bestlap");
            int completed = int.Parse(parsed.Get("completed"));
            _dataStore.HandleIncomingData(username, lastlap, bestlap, completed);
            return LeaderBoard();
        }
    }
}
