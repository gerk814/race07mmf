﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Race07QualyServer
{
    public enum CommandType
    {
        AddLap,
        LeaderBoard
    }

    public static class Extension
    {
        public static CommandType ParseEnum(this string text)
        {
            if (text.ToLower() == "addlap")
            {
                return CommandType.AddLap;
            }
            else if (text.ToLower() == "leaderboard")
            {
                return CommandType.LeaderBoard;
            }
            return CommandType.LeaderBoard;
        }
    }
}
