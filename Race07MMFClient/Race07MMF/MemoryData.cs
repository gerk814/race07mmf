﻿namespace Race07MMF
{
    public struct MemoryData
    {
        public float userinput1;
        public float userinput2;
        public float userinput3;
        public float userinput4;
        public float userinput5;
        public float userinput6;
        public float rpm;
        public float maxEngineRPS;
        public float fuelPressure;
        public float fuel;
        public float fuelCapacityLiters;
        public float engineWaterTemp;
        public float engineOilTemp;
        public float engineOilPressure;
        public float carSpeed;
        public int numberOfLaps;
        public int completedLaps;
        public float lapTimeBest;
        public float lapTimePrevious;
        public float lapTimeCurrent;
        public int position;
        public int numCars;
        public int gear;
        public float tirefrontleft1;
        public float tirefrontleft2;
        public float tirefrontleft3;
        public float tirefrontright1;
        public float tirefrontright2;
        public float tirefrontright3;
        public float tirerearleft1;
        public float tirerearleft2;
        public float tirerearleft3;
        public float tirerearright1;
        public float tirerearright2;
        public float tirerearright3;
        public int numPenalties;
        public float carCGLocX;
        public float carCGLocY;
        public float carCGLocZ;
        public float pitch;
        public float yaw;
        public float roll;
        public float lateral;
        public float vertical;
        public float longitudinal;
    }
}
