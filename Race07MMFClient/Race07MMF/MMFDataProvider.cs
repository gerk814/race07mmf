﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Race07MMF
{
    public class MMFDataProvider
    {
        public MMFData GetDataFromMMFFile()
        {
            MMFData data = new MMFData("", "", 0);
            try
            {
                MemoryMappedFile mmf = MemoryMappedFile.OpenExisting("$Race$");
                MemoryMappedViewAccessor accessor = mmf.CreateViewAccessor();
                MemoryData value;
                accessor.Read(0, out value);

                string last = SecToLaptimeFormat(value.lapTimePrevious);
                string best = SecToLaptimeFormat(value.lapTimeBest);
                int completed = value.completedLaps;
                // print it to the console  
                Console.WriteLine("Last lap time: {0}", last);
                Console.WriteLine("Best lap time: {0}", best);
                Console.WriteLine("Completed laps {0}", completed);

                data.LastLap = last;
                data.BestLap = best;
                data.CompletedLaps = completed;

                // dispose of the memory-mapped file object and its accessor  
                accessor.Dispose();
                mmf.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return data;
        }

        public static string SecToLaptimeFormat(float time)
        {
            int minutes = (int)time / 60;
            float secMs = time - (minutes * 60);
            bool needFilling0 = secMs < 10.0;
            string zero = needFilling0 ? "0" : "";
            string ret = $"{minutes}:{zero}{secMs}";
            return ret.Replace(",", ".");
        }
    }

    public class MMFData
    {
        public string LastLap { get; set; }

        public string BestLap { get; set; }

        public int CompletedLaps { get; set; }

        public MMFData(string lastLap, string bestLap, int completedLaps)
        {
            LastLap = lastLap;
            BestLap = bestLap;
            CompletedLaps = completedLaps;
        }
    }
}
