﻿using Race07MMF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MMFClientUI
{
    public class HttpPollExecuterClient
    {
        private readonly string _url = ConfigurationManager.AppSettings["serverUrl"]; //"http://91.83.32.186:80/"; //"http://192.168.0.152:8000/";
        private readonly string _username = ConfigurationManager.AppSettings["username"];
        private readonly ObservableCollection<LapData> _lapDataList;

        public HttpPollExecuterClient(ObservableCollection<LapData> lapData)
        {
            _lapDataList = lapData;
        }

        public void BeginPoll(int delay_sec)
        {
            Console.WriteLine($"Start polling with {delay_sec}s intervals");
            Task.Run(() =>
            {
                while (true)
                {
                    GetData();
                    System.Threading.Thread.Sleep(delay_sec * 1000);
                }
            });
        }

        private void GetData()
        {
            try
            {
                //get data from disk
                string lastlap = "1:16.832";
                string bestlap = "1:12.103";
                int completed = 3;
                /*
                MMFDataProvider dataProvider = new MMFDataProvider();
                var memorydata = dataProvider.GetDataFromMMFFile();
                lastlap = memorydata.LastLap;
                bestlap = memorydata.BestLap;
                completed = memorydata.CompletedLaps;
                */
                WebClient client = new WebClient();
                client.QueryString.Add("command", "addlap");
                client.QueryString.Add("username", _username);
                client.QueryString.Add("lastlap", lastlap);
                client.QueryString.Add("bestlap", bestlap);
                client.QueryString.Add("completed", completed.ToString());
                string response = client.DownloadString(_url);
                string[] lapDataList = response.Split('#');

                foreach (var data in lapDataList)
                {
                    if (string.IsNullOrEmpty(data)) continue;
                    string[] row = data.Split(';');
                    string uname = row[0];
                    string llap = row[1];
                    string blap = row[2];
                    if (_lapDataList.Any(l => l.UserName == uname))
                    {
                        var item = _lapDataList.First(l => l.UserName == uname);
                        if (item.BestLap != blap)
                        {
                            item.BestLap = blap;
                        }
                        if (item.LastLap != llap)
                        {
                            item.LastLap = llap;
                        }
                    }
                    else
                    {
                        var newData = new LapData { UserName = uname, LastLap = llap, BestLap = blap };
                        Application.Current.Dispatcher.Invoke(() => _lapDataList.Add(newData));
                    }
                }
            } 
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
