﻿using GameOverlay.Drawing;
using GameOverlay.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMFClientUI
{
    public class OverlayUI : IDisposable
    {
        private readonly GraphicsWindow _window;

        public OverlayUI()
        {
            var gfx = new Graphics()
            {
                PerPrimitiveAntiAliasing = true,
                TextAntiAliasing = true
            };
            _window = new GraphicsWindow(100, 100, 350, 180, gfx)
            {
                IsTopmost = true,
                IsVisible = true
            };

            _window.DestroyGraphics += _window_DestroyGraphics;
            _window.DrawGraphics += _window_DrawGraphics;
            _window.SetupGraphics += _window_SetupGraphics; 
        }

        private void _window_SetupGraphics(object sender, SetupGraphicsEventArgs e)
        {
        }

        private void _window_DrawGraphics(object sender, DrawGraphicsEventArgs e)
        {
            var gfx = e.Graphics;
            var text = "tesztszöveg";
            gfx.DrawText(gfx.CreateFont("Consolas", 14), gfx.CreateSolidBrush(0, 0, 255), 20, 20, text);
        }

        private void _window_DestroyGraphics(object sender, DestroyGraphicsEventArgs e)
        {
        }

        public void Run()
        {
            _window.Create();
            _window.Join();
        }

        ~OverlayUI()
        {
            Dispose(false);
        }

        private bool _disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                _window.Dispose();
                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
