﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;

namespace MMFClientUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            bool showUI = bool.Parse(ConfigurationManager.AppSettings["showUI"]);

            ViewModel viewModel = new ViewModel();
            if (showUI)
            {
                MainWindow window = new MainWindow(viewModel);
                window.Show();
            }

            HttpPollExecuterClient client = new HttpPollExecuterClient(viewModel.LapDataList);
            client.BeginPoll(10);
            


            //   Process[] processes = Process.GetProcessesByName("Race_Steam");
            // Process p = processes.FirstOrDefault();
            // IntPtr windowHandle;
            //   if (p != null)
            //  {
            //     windowHandle = p.MainWindowHandle;

            /* using (var ui = new OverlayUI())
             {
                 ui.Run();
             }*/
            //}
        }
    }
}
