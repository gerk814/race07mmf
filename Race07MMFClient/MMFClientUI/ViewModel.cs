﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMFClientUI
{
    public class ViewModel : ViewModelBase
    {
        private ObservableCollection<LapData> _lapDataList = new ObservableCollection<LapData>();
        public ObservableCollection<LapData> LapDataList
        {
            get { return _lapDataList; }
            set { _lapDataList = value; OnPropertyChanged(nameof(LapDataList)); }
        }
    }

    public class LapData : ViewModelBase
    {
        private string _userName = "";
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; OnPropertyChanged(nameof(UserName)); }
        }

        private string _bestLap = "";
        public string BestLap
        {
            get { return _bestLap; }
            set { _bestLap = value; OnPropertyChanged(nameof(BestLap)); }
        }

        private string _lastLap = "";
        public string LastLap
        {
            get { return _lastLap; }
            set { _lastLap = value; OnPropertyChanged(nameof(LastLap)); }
        }
    }
}
