import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageA } from './Pages/pageA/pageA.component';
import { PageB } from './Pages/pageB/pageB.component';


const routes: Routes = [
  { path: "a", component: PageA },
  { path: "b", component: PageB },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
