import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'sample-project';
  public link_a: string = 'a';
  public link_c: string = 'a';

  ngOnInit() {
    this.link_c = 'b';
  }

  switchLink() {
    if (this.link_c == 'a') {
      this.link_c = 'b';
    }
    else {
      this.link_c = 'a';
    }
  }
}
