import { AfterViewInit, Component } from '@angular/core';
import { WebService } from 'src/app/services/web.service';

@Component({
  selector: 'page-b',
  templateUrl: './pageB.component.html'
})

export class PageB implements AfterViewInit {
  public laps_format: Laptime[];
  public displayedColumns: string[] = ['Pos', 'Name', 'Best', 'Last'];

  constructor(private webService: WebService) { }

  ngAfterViewInit() {
    console.log("B on init");
    this.poll();
  }

  async poll() {
    while (true) {
      this.getLap();
      await this.delay(10000);
    }
  }

  async getLap() {
    this.webService.getData();
    await this.delay(100);
    var laps = this.webService.laps.split('#');

    this.laps_format = new Array(laps.length - 1);
    for (let i = 1; i < laps.length; i++) {
      var data = laps[i].split(';');
      var playerdata = new Laptime(data[0], data[1], data[2]);
      this.laps_format[i - 1] = playerdata;
    }
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

export class Laptime {
  constructor(public user: string, public lastlap: string, public bestlap: string) { }
}