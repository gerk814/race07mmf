import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WebService {
  constructor(private http: HttpClient) { }

  public laps: string;

  getData() {
    this.http.get("/api/?command=leaderboard", { responseType: 'text' }).subscribe(x => {
      this.laps = x;
    });
  }
}